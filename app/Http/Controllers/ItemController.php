<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use \App\Category;
use Session;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // get item list
        $items = Item::all();
        //dd($items);
        return view('items.index', compact('items')); //view send item list
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // Get list of categories
        $categories = Category::all()->sortByDesc('name');

        //dd($catergories);
        return view('items.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request);
        //dd($request->file('image')->getClientOriginalName());
        $timestamp = \Carbon\Carbon::now()->format('Ymdhms');
        $image = $request->file('image');
        $filename = $timestamp . $image->getClientOriginalName();
        $image->move("images", $filename);

        $item = new Item;

        $item->name = $request->item_name;
        $item->price = $request->price;
        $item->description = $request->description;
        $item->category_id = $request->category_id;
        $item->image = $filename;

        $item->save();

        Session::flash('successmessage', 'Item saved successfully!, details as follow '.$item->name.', '.$item->price.', '.$item->description.', '.$item->category_id);
        return redirect('/items');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return "Item displayed";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $item = Item::find($id);
        $categories = Category::all();
        return view('items.edit', compact('item', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $item = Item::find($id);
        $item->name = request('item_name');
        $item->price = request('price');
        $item->description = request('description');
        $item->category_id = request('category_id');


         $file = $request->file('image');
        // dd(!$file);
        if($file){
           
            $timestamp = \Carbon\Carbon::now()->format('Ymdhms');
            $filename = $file->getClientOriginalName();
            $file->move('images', $filename);
            $item->image= $filename;

        }

        $item->save();
        return view();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $item= Item::find($id);
        $item->delete();

        return redirect('/items')->with('successmessage', 'Item deleted successfully');
    }

    public function catalog()
    {
        $items = Item::all();
        return view('items.catalog', compact('items'));
    }
}
