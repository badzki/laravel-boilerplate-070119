<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use \App\Item;
use \App\Order;
use Auth;
use Illuminate\Support\Str;

class CartController extends Controller
{

	public function index(){

		$cart = collect(Session::get('cart'));



		$cart_items = $cart->map(function($quantity, $item_id){
			$item = Item::find($item_id);
			$item->quantity = $quantity;

			return $item;
		});

		// dd($cart_items);
		return view('cart.index', compact('cart_items'));
	}


    public function add($item, $quantity)
    {	

    	// dd(Session::get('cart'));

    	if(!Session::has('cart')){
    		Session::put('cart',[]);
    	}

    	$cart= collect(Session::get('cart'));

    	if($cart->has($item)){
    		$cart[$item] = (int)$cart[$item] + (int)$quantity;
    	} else {
    		$cart[$item] = $quantity;
    	}

    	Session::put('cart', $cart);

    	return $cart->count();
    	// return $cart;
    }

    // public function add()
    // {


    // 	return request('item').' '.request('quantity');
    // }



    // second way for add function
    // public function add($item, $quantity)
    // {
    // 	return $item.' '.$quantity
    // }


    public function checkout()
    {
    	// dd(Auth::user()->id);

    	$user_id = Auth::user()->id;
    	// $trans_code = \Carbon\Carbon::now()->format('Ymdhms') . Str::random(12);

    	$trans_code = Str::uuid();

    	$order = new Order;
    	$order->user_id = $user_id;
    	$order->trans_code = $trans_code;

    	$order->save(); 

    	//inserting in the database
    	$cart = Session::get('cart');
    	foreach($cart as $item_id => $quantity){
    		$item = Item::find($item_id);
    		$order->items()->attach($item_id, [
    			"quantity" => $quantity,
    			"price" => $item->price
    		]);
    	}

    	$order->save();

    	Session::put('cart', []);
    	// Session::flush('trans_code', $trans_code);

    	return view('cart.success', compact('trans_code'));

    	// dd((string)$trans_code);
    	// (string) -> data casting
    }
}
