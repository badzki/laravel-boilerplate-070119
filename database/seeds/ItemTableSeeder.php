<?php

use Illuminate\Database\Seeder;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('items')->insert(
        	[
        		[
        		"name" => "Generic Keyboard",
        		"price" => 450.00,
        		"description" => "Standard Keyboard for daily use.",
        		"image" => "image.jpg",
        		"category_id" => 1
        		],
        		[
        		"name" => "Generic Monitor",
        		"price" => 2500.00,
        		"description" => "Standard LCD Monitor for daily use.",
        		"image" => "image2.jpg",
        		"category_id" => 2
	        	],
	        	[
        		"name" => "Intel Dual Core Processor",
        		"price" => 1500.00,
        		"description" => "Average processor.",
        		"image" => "image3.jpg",
        		"category_id" => 3
        		]
        	]
        );
    }
}
