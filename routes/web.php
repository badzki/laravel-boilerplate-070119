<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/home', "HomeController@index");
	
Auth::routes();

Route::get('/home', "HomeController@index")->name('home');
//items

Route::get('/catalog', "ItemController@catalog");   
Route::resource('/items', "ItemController");			 // shortcut for items routes
// Route::get('/items', "ItemController@index");            // item list
//Route::get('/items/{item}', "ItemController@show");      // specific item
// Route::post('/items', "ItemController@store");		     // add items
// Route::get('/items/create', "ItemController@create");    // create form
// Route::get('/items/{id}/edit', "ItemController@edit"); // get specific item
// Route::put('/items/{item}', "ItemController@update");	 // edit specific items
// Route::delete('/items/{id}', "ItemController@destroy"); // delete items



Route::post('/cart/{item}/{quantity}', 'CartController@add');
Route::get('/cart', 'CartController@index');

Route::get('/logout', 'HomeController@logout')->name('logout');

Route::get('/checkout', 'CartController@checkout');

