@extends('layouts.app')

@section('content')
	<div class="container">
		<form action="/items" method="POST" enctype="multipart/form-data"> {{-- used to upload image --}}
			@csrf
			<label for="item_name">Name</label>
			<input type="text" class="form-control" name="item_name">

			<label for="price">Price</label>
			<input type="number" class="form-control" name="price">

			<label for="description">Description</label>
			<input type="text" class="form-control" name="description">

			<label for="category">Category</label>
			<select class="form-control" name="category_id">
				<option value=""></option>
				@foreach($categories as $category)
					<option value="{{ $category->id }}">{{ $category->name }}</option>
				@endforeach
			</select>

			<label for="image">Image</label>
			<input type="file" class="form-control" name="image">

			<button class="btn btn-primary">Add Item</button>
		</form>
	</div>
@endsection