@extends('layouts.app')

@section('content')

@if(Session::has('successmessage'))
<div class="alert alert-success" role="alert">
	{{ Session::get('successmessage') }}
</div>
@endif

<div class="container">
	<div class="d-inline">
		<div class="d-inline">
			<h3 class="float-left">Showing All Items</h3>
		</div>
		<a href="/items/create" class="btn btn-outline-info float-right my-4 d-inline"> Add New Item</a>
	</div>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Image</th>
				<th>Name</th>
				<th>Price</th>
				<th>Description</th>
				<th>Category</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			@foreach($items as $item)
			<tr>
				<td>
					<img src="{{ asset('images/'. $item->image) }}" alt="" style="width: 100px; max-height: 100px;">
				</td>
				<td>{{ $item->name }}</td>
				<td>{{ $item->price }}</td>
				<td>{{ $item->description }}</td>
				<td>{{ $item->category->name }}</td>
				<td>
					<a href="/items/{{$item->id}}/edit" class="btn btn-info">Edit</a>
				</td>
				<td>
					<form action="/items/{{ $item->id }}" method="POST">

						@csrf
						@method('DELETE')
						<button class="btn btn-danger">Delete</button>
						

						
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection