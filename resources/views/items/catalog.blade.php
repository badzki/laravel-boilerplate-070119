@extends('layouts.app')

@section('content')
<div class="container">
	<div class="card-columns">

		@foreach($items as $item)
		<div class="card" style="width: 18rem;">

			

			<img src="{{ asset('images/' . $item->image) }}" class="card-img-top" alt="Card image cap" style="width:18rem; max-height: 30rem;">
			<div class="card-body">
				<p>{{ $item->name }}</p>
				<small>{{ $item->description }}</small>
				<p>{{ $item->price }}</p>
			</div>
			<div class="card-footer">
				<div class="input-group mb-3">
					{{-- <form action="/cart/{{ $item->id }}/{quantity}" method="POST">
						@csrf --}}
						<input type="text" class="form-control" name="quantity" placeholder="Quantity" aria-label="Quantity" aria-describedby="basic-add-quantity">
						<div class="input-group-append">
							<button class="btn btn-primary text-white add-cart" data-item_id="{{ $item->id }}">Add to Cart</button>
						</div>
					{{-- </form> --}}
				</div>
			</div>

		</div>
		@endforeach
	</div>	
</div>




@endsection