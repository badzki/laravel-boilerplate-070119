@extends('layouts.app')

@section('content')
	<div class="container">
		<form action="/items/{{ $item->id }}" method="POST" enctype="multipart/form-data"> {{-- used to upload image --}}
			@csrf
			@method('PUT')

			<label for="item_name">Name</label>

			<input type="text" class="form-control" name="item_name" value="{{ $item->name }}">

			<label for="price">Price</label>
			<input type="number" class="form-control" name="price" value="{{ $item->price }}">

			<label for="description">Description</label>
			<input type="text" class="form-control" name="description" value="{{ $item->description }}">

			<label for="category">Category</label>
			<select class="form-control" name="category_id">
				<option value=""></option>
				@foreach($categories as $category)
					@if($category->id === $item->category_id)
						<option value="{{ $category->id }}" selected> {{ $category->name }} </option>
					@else
						<option value="{{ $category->id }}"> {{ $category->name }} </option>
					@endif

				@endforeach

				
			</select>

			<label for="image">Image</label>
			<input type="file" class="form-control" name="image" value="{{ $item->image }}">

			<button class="btn btn-primary">Update Item</button>
		</form>
	</div>
@endsection