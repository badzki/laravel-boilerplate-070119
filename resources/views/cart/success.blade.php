@extends('layouts.app')

@section('content')

<div class="container">
	<h1>Order Successful</h1>
	<p>Your Transaction code is <code>{{ $trans_code }}</code>
	. Please wait for the delivery!</p>

	<br>
	<br>
	<div class="text-center">
		<a href="/catalog" class="link"> Continue Shopping</a>
	</div>
</div>
@endsection




