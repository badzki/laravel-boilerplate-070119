@extends('layouts.app')


@section('content')
<div class="container">

	<a href="/checkout" class="btn btn-primary py-3">Checkout</a>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Item</th>
				<th>Price</th>
				<th>Quantity</th>
				<th>Subtotal</th>

			</tr>
		</thead>


		<tbody>
			@foreach($cart_items as $item)
			<tr>
				<td>{{ $item->name }}</td>
				<td>{{ $item->price }}</td>
				<td>{{ $item->quantity }}</td>
				<td>{{ $item->quantity * $item->price }}</td>
			</tr>

			@endforeach
		</tbody>


		<tfoot>
			<tr>
				<td colspan="3" class="text-right">Total</td>
				<td>TOTAL</td>
			</tr>
		</tfoot>
	</table>

</div>

@endsection